# MiniService for OnlyOffice v.0.0.1 #

OS - Ubuntu Server 14.04

Python version - 3.5

## How to install: ##
1) git clone https://timeis@bitbucket.org/timeis/miniservice-for-oo.git

2) sudo apt-get install python-virtualenv

3) cd ./miniservice-for-oo/

4) installing python3.5:

   - sudo add-apt-repository ppa:fkrull/deadsnakes
   - sudo apt-get update
   - sudo apt-get install python3.5

and also install python3-dev:

   - sudo apt-get install python3-dev

5) virtualenv ./env -p python3.5

6) in ./source/config.py change:

   - SERVER_ADDRESS it is your external ip
   - PORT as you wish

7) ./env/bin/python3.5 ./source/setup.py install

## How to run: ##
./env/bin/python3.5 ./source/main.py