#!/usr/bin/python3.5

import urllib.parse

from aiohttp import web

import source.config as config
from source.upload_file import upload_file_to_cloud
from source.upload_file import InvalidResponse
from source.convert_url import get_converted_url


async def get_handle(request):
    return web.Response(text='miniservice for OnlyOffice v 0.0.1')

extension = {
    'docx': ['doc', 'epub', 'html', 'mht', 'odt', 'rtf', 'txt', 'xps'],
    'xlsx': ['csv', 'ods', 'xls'],
    'pptx': ['odp', 'pps', 'ppsx', 'ppt']
}

async def post_handle(request):
    req_body = await request.json()

    if 'status' in req_body and req_body['status'] == 2:
        token = request.GET['token']
        path_in_cloud = urllib.parse.unquote(request.GET['path'])

        download_ext = req_body['url'].split('.')[-1]
        cur_ext = urllib.parse.unquote(request.GET['path']).split('.')[-1]
        # checking if download file extension does not match file extension on cloudike
        if not download_ext == cur_ext:
            try:
                req_body['url'] = get_converted_url(req_body['url'], download_ext, cur_ext, req_body['key'])
            except InvalidResponse as exc:
                print(exc)
                for key, value in extension.items():
                    if cur_ext in value:
                        path_in_cloud = path_in_cloud+'.'+key
        upload_file_to_cloud(req_body['url'], token, path_in_cloud)

    return web.Response(text="{\"error\":0}")

app = web.Application()
app.router.add_get('/', get_handle)
app.router.add_post('/', post_handle)

web.run_app(app, host=config.SERVER_ADDRESS, port=config.PORT)
