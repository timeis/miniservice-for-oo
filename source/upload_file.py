#!/usr/bin/python3.5

import time
import requests
import json
from contextlib import closing

from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class ServiceException(Exception):
    def __init__(self, message, response):
        if response:
            message += "\n" + str(response)
        super(ServiceException, self).__init__(message)
        self.response = response


class ConnectionException(ServiceException):
    pass


class InvalidStatus(ServiceException):
    pass


class InvalidResponse(ServiceException):
    pass


def manage_response(response):
    parsed = response.text
    try:
        parsed = json.loads(response.text)
    except ValueError:
        # logger.error("Can't parse response for request |%s [%s] %s| [%s]: %s |||",
        #                   response.status_code, parsed[:400])
        raise InvalidResponse("Can't parse response", parsed[:400])
    return parsed


def files_create(token, path_in_cloud):
    headers = {
        "Accept": "application/json",
        "User-Agent": "MiniServiceForOnlyOffice/0.0.1",
        "Mountbit-Auth": token,
        "Mountbit-Request-Id": str(int(time.time()))
    }
    params = {
        "path": path_in_cloud,
        "overwrite": True
    }
    url = 'https://cloudike.biz/api/1/files/create/'

    # logger.info("Request: %s [%s] %s %s" % (method, url, parameters, headers))
    for attempt in range(3):
        try:
            response = requests.request('POST',
                                        url=url,
                                        headers=headers,
                                        data=params)
        except requests.RequestException as exc:
            # logger.info("Cannot send request, attempt %d: %s", attempt + 1, exc)
            pass
        else:
            break
    else:
        # logger.error("Cannot send request %s (%s)" %(url, method))
        raise ConnectionException("Cannot connect to:", url)

    if not response.status_code == 200:
        raise InvalidStatus("Status: %s, but expected on of: 200 Request: %s POST %s %s" % (response.status_code, url,
                                                                                            params, headers),
                            response.text[:400])
    return manage_response(response)


def get_file(download_url):
    with closing(requests.get(download_url, stream=True)) as res:
        res.raise_for_status()
        file_stream = res.content
        return file_stream


def send_file(url, file_, parameters, headers, method='POST'):
        files = {"file": ('file_name', file_)}
        response = requests.request(method, url,
                                    headers=headers,
                                    data=parameters,
                                    files=files,
                                    verify=False,
                                    allow_redirects=False)
        response.raise_for_status()
        return response.text


def files_confirm(self, path, user_id, temp_name, overwrite=None, version=None, size=None, checksum=None,
                  expect_status=200, parse=True):
    params = {'user_id': user_id,
              'temp_name': temp_name,
              'overwrite': overwrite,
              'version': version,
              'size': size,
              'checksum': checksum}
    return self.user_send(self.METHOD_POST, 'files/confirm/' + path,
                          parameters=params,
                          expect_status=expect_status,
                          parse=parse)


def upload_file_to_cloud(update_url, token, path_in_cloud):
    resp_files_create = files_create(token, path_in_cloud)
    headers = resp_files_create['headers']
    if resp_files_create['parameters'] == {}:
        parameters = headers
        headers = None
    else:
        parameters = resp_files_create['parameters']

    # logger.info("Getting file from OnlyOffice: %s" % (update_url))
    temp_filename = get_file(update_url)

    # logger.info("Sending file: [%s] %s %s" % (resp_files_create['url'], headers, parameters))
    send_file(resp_files_create['url'], temp_filename, parameters, headers)

    # logger.info("Confirming file: %s" % (resp_files_create['confirm_url']))
    requests.request('POST', resp_files_create['confirm_url'])
    print('Done with:'+path_in_cloud)
