#!/usr/bin/python3.5

import requests
import re

from source.upload_file import ConnectionException
from source.upload_file import InvalidStatus
from source.upload_file import InvalidResponse


def send_request_to_convert(download_url, from_ext, to_ext, key):
    server_addr = download_url.split('/')[2]
    params = {
        'filetype': from_ext,
        'key': key,
        'outputtype': to_ext,
        'url': download_url.split('?')[0]
    }
    send_url = 'http://'+server_addr+'/ConvertService.ashx'
    # logger.info("Request: %s [%s] %s" % ("GET", send_url, params))
    for attempt in range(3):
        try:
            response = requests.request('GET', url=send_url, params=params)
        except requests.RequestException as exc:
            # logger.info("Cannot send request, attempt %d: %s", attempt + 1, exc)
            pass
        else:
            if 'Error' in response.text:
                raise InvalidResponse("Look at https://api.onlyoffice.com/editors/conversionapi. Error when convert:",
                                      response.text)
            break
    else:
        # logger.error("Cannot send request %s (%s)" %('GET', send_url))
        raise ConnectionException("Cannot connect to:", send_url)

    if not response.status_code == 200:
        raise InvalidStatus("Status: %s, but expected on of: 200 Request: %s GET %s" % (response.status_code,
                                                                                        send_url, params),
                            response.text[:400])

    return response.text


def get_response_url(xml):
    converted_url = re.split('<[/]?FileUrl>', xml)[1]
    return converted_url


def get_converted_url(download_url, from_ext, to_ext, key):
    xml = send_request_to_convert(download_url, from_ext, to_ext, key)
    converted_url = get_response_url(xml)
    return converted_url
