from setuptools import setup, find_packages


install_requires = ["aiohttp",
                    "async-timeout",
                    "asyncio",
                    "chardet",
                    "multidict",
                    "requests",
                    ]
packages = find_packages(".")

setup(name="mountbit_miniservice_for_onlyoffice",
      version="0.0.1",
      packages=packages,
      install_requires=install_requires,
      classifiers=[
          "Development Status :: 1 - Planning",
          "Environment :: Web Environment",
          "License :: Other/Proprietary License",
          "Operating System :: Unix",
          "Programming Language :: Python :: 2.7",
          "Topic :: Tests :: Acceptance test",
      ],
      entry_points={
          'console_scripts':
              [
                  'main=main:main',
              ]}
      )
